# JavaScript & TypeScript

- [Using data attributes](https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes)

## npm tricks

### Run multiple commands at once

```json
"scripts": {
    "start:build": "tsc -w",
    "start:run": "nodemon build/index.js",
    "start": "concurrently npm:start:*"
}
```

## npm packages

- [nodemon - monitor for any changes in your node.js application and automatically restart the server](https://github.com/remy/nodemon)
- [concurrently - run commands concurrently](https://github.com/kimmobrunfeldt/concurrently)

## TypeScript tricks

### Initialize (generate tsconfig.json file)

```sh
tsc --init
```

## Libraries

- [particles.js - A lightweight JavaScript library for creating particles](https://github.com/VincentGarreau/particles.js/)
- [Chart.js - Simple yet flexible JavaScript charting for designers & developers](https://www.chartjs.org/)
- [particles.js](https://vincentgarreau.com/particles.js/)
- [lax.js - Simple & light weight vanilla javascript plugin to create smooth & beautiful animations when you scroll](https://github.com/alexfoxy/laxxx)
- [lax.js - Video Tutorial](https://www.youtube.com/watch?v=jaVy3SCibJw&t)
- [Web APIs](https://developer.mozilla.org/en-US/docs/Web/API)
- [barba.js - Create badass fluid and smooth transitions](https://barba.js.org/)
- [swup - Complete, flexible, extensible and easy to use page transition library](https://swup.js.org/)
- [Rellax - buttery smooth, super lightweight, vanilla javascript parallax library.](https://dixonandmoe.com/rellax/)
- [Anime.js - animates using CSS properties, SVG, DOM attributes and JavaScript](https://animejs.com/)
- [Masonry - Cascading grid layout library](https://masonry.desandro.com/)
- [chart.xkcd - chart library that plots "sketchy", "cartoony" or "hand-drawn" styled charts](https://github.com/timqian/chart.xkcd)

## Canvas

[Canvas arc with linear gradient](https://codepen.io/fiatuno/pen/QWWMONE)
